const { gql } = require('apollo-server');
const accountTypeDefs = gql`
type Account {
    username: String!
    balance: Int!
}
extend type Query {
    accountByUsername(username: String!): Account
    transactionByUsername(username: String!): [Account]
}

`;
module.exports = accountTypeDefs;