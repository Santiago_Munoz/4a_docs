const authTypeDef = require("./django_type_def")
const accountTypeDefs = require("./java_type_def")
const schemas = [authTypeDef, accountTypeDefs]
module.exports = schemas