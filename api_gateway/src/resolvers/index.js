const accountResolver = require('./java_resolver');
const authResolver = require('./django_resolver');

const lodash = require('lodash');
const resolvers = lodash.merge(accountResolver, authResolver);
module.exports = resolvers;